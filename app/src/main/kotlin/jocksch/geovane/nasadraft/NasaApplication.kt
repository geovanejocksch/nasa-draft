package jocksch.geovane.nasadraft

import android.app.Application
import com.feature.donki.di.donkiServiceModule
import org.koin.core.context.startKoin

class NasaApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            modules(
                listOf(
                    donkiServiceModule
                )
            )
        }
    }
}