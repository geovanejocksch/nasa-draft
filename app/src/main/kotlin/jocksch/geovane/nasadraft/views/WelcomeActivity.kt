package jocksch.geovane.nasadraft.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import jocksch.geovane.nasadraft.R
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        prepareListeners()
    }

    private fun prepareListeners() {
        exploreButton.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }
}
