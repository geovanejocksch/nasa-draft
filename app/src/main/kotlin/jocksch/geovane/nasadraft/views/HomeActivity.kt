package jocksch.geovane.nasadraft.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.feature.donki.views.CoronalMassEjectionActivity
import jocksch.geovane.nasadraft.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        prepareListeners()
    }

    private fun prepareListeners() {
        asteroidsLayout.setOnClickListener {
            // Open Asteroids activity
        }

        interplanetaryShockLayout.setOnClickListener {
            // Open Interplanetary Shock activity
        }

        sunLayout.setOnClickListener {
            // Open Sun activity
        }

        donkiLayout.setOnClickListener {
            startActivity(Intent(this, CoronalMassEjectionActivity::class.java))
        }
    }
}