package com.example.donki.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.feature.donki.models.CoronalMassEjection
import com.feature.donki.repositories.DonkiRepository
import com.feature.donki.viewmodels.CoronalMassEjectionViewModel
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class CoronalMassEjectionViewModelTest {

    private lateinit var repositoryMock: DonkiRepository
    private lateinit var viewModel: CoronalMassEjectionViewModel
    private lateinit var coroutineContextMock: CoroutineContext

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        repositoryMock = mock()
        coroutineContextMock = Dispatchers.Unconfined
        viewModel = CoronalMassEjectionViewModel(repositoryMock, coroutineContextMock)
    }

    @Test
    fun `Get coronal mass ejections, then assert that livedata is filled`() {

        // Arrange
        val expectedResult = listOf(
            CoronalMassEjection(
                activeRegionNum = 1,
                activityID = "1231-4134-d131",
                catalog = "First Catalog",
                cmeAnalyses = listOf(),
                instruments = listOf(),
                linkedEvents = listOf(),
                note = "Here some note",
                sourceLocation = "Source Location",
                startTime = "2018-01-01"
            )
        )

        whenever(
            runBlocking(coroutineContextMock) {
                repositoryMock.getCoronalMassEjectionList(
                    any(),
                    any()
                )
            }
        ).thenReturn(
            expectedResult
        )

        // Act
        viewModel.getCoronalMassEjectionViewModel("", "")

        // Assert
        assertEquals(expectedResult, viewModel.coronalMassEjectionList.value)
    }

    @Test
    fun `Get coronal mass ejections, then handle error response`() {

        // Arrange
        val exceptionMessage = "Error description"

        whenever(
            runBlocking(coroutineContextMock) {
                repositoryMock.getCoronalMassEjectionList(
                    any(),
                    any()
                )
            }
        ).then {
            throw Exception(exceptionMessage)
        }

        // Act
        viewModel.getCoronalMassEjectionViewModel("", "")

        // Assert
        assertEquals(exceptionMessage, viewModel.error.value)
    }
}