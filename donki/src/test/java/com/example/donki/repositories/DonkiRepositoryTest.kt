package com.example.donki.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.feature.donki.datasources.DonkiDataSource
import com.feature.donki.models.CoronalMassEjection
import com.feature.donki.repositories.DonkiRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class DonkiRepositoryTest {

    private lateinit var datasourceMock: DonkiDataSource
    private lateinit var repository: DonkiRepository

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        datasourceMock = mock()
        repository = DonkiRepository(datasourceMock)
    }

    @Test
    fun `Get coronal mass ejection, then assert suspend function returns`() {

        // Arrange
        val expectedResult = listOf(
            CoronalMassEjection(
                activeRegionNum = 1,
                activityID = "1231-4134-d131",
                catalog = "First Catalog",
                cmeAnalyses = listOf(),
                instruments = listOf(),
                linkedEvents = listOf(),
                note = "Here some note",
                sourceLocation = "Source Location",
                startTime = "2018-01-01"
            )
        )

        whenever(
            runBlocking {
                datasourceMock.getCoronalMassEjectionList(any(), any())
            }
        ).thenReturn(expectedResult)

        // Act
        runBlocking {
            val result = repository.getCoronalMassEjectionList("", "")
            assertEquals(expectedResult, result)
        }
    }
}