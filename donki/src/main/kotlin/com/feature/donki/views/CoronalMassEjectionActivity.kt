package com.feature.donki.views

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.donki.R
import com.feature.donki.models.CoronalMassEjection
import com.feature.donki.viewmodels.CoronalMassEjectionViewModel
import kotlinx.android.synthetic.main.activity_coronal_mass_ejection.*
import org.koin.android.viewmodel.ext.android.viewModel

class CoronalMassEjectionActivity : AppCompatActivity() {

    private val coronalMassEjectionViewModel: CoronalMassEjectionViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_coronal_mass_ejection)

        preparateObservers()
        preparateViewModel()
    }

    private fun preparateViewModel() {
        // Mocked values provided, UI not implemented yet
        coronalMassEjectionViewModel.getCoronalMassEjectionViewModel("2017-06-06", "2018-01-01")
    }

    private fun preparateObservers() {
        coronalMassEjectionViewModel.coronalMassEjectionList.observe(this, Observer { list ->
            if (list == null) {
                return@Observer
            }
            prepareRecycler(list)
            Log.d("CME", list.joinToString { it.activityID })
        })

        coronalMassEjectionViewModel.error.observe(this, Observer { error ->
            if (error == null) {
                return@Observer
            }

            cmeTV.text = error
            Log.d("CME", "Error retrieving $error")
        })
    }

    private fun prepareRecycler(items: List<CoronalMassEjection>) {
        cmeTV.text = items.size.toString()
    }
}