package com.feature.donki.repositories

import com.feature.donki.datasources.DonkiDataSource
import com.feature.donki.models.CoronalMassEjection
import kotlinx.coroutines.Deferred

class DonkiRepository(private val dataSource: DonkiDataSource) {

    suspend fun getCoronalMassEjectionList(
        startDate: String,
        endDate: String
    ): List<CoronalMassEjection> {
        return dataSource.getCoronalMassEjectionList(startDate, endDate)
    }
}