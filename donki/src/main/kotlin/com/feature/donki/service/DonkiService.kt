package com.feature.donki.service

import com.example.donki.BuildConfig
import com.feature.donki.models.CoronalMassEjection
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface DonkiService {

    @GET("DONKI/CME")
    suspend fun getCoronalMassEjection(
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("api_key") apiKey: String = BuildConfig.API_KEY
    ): List<CoronalMassEjection>
}