package com.feature.donki.service

import com.example.donki.BuildConfig
import com.example.donki.BuildConfig.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun <T> getApi(serviceClass: Class<T>): T {

    // Configure okHttp client
    val client = OkHttpClient.Builder()
    client.readTimeout(6000, TimeUnit.SECONDS)
    client.connectTimeout(6000, TimeUnit.SECONDS)

    // Log only when debug
    if (BuildConfig.DEBUG) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(logging)
    }

    // Build retrofit
    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    return retrofit.create(serviceClass)
}