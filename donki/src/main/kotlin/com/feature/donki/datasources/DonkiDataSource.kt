package com.feature.donki.datasources

import com.feature.donki.models.CoronalMassEjection
import com.feature.donki.service.DonkiService

class DonkiDataSource(private val service: DonkiService) {


    suspend fun getCoronalMassEjectionList(
        startDate: String,
        endDate: String
    ): List<CoronalMassEjection> {
        return service.getCoronalMassEjection(startDate, endDate)
    }
}