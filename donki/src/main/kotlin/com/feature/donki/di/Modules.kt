package com.feature.donki.di

import com.feature.donki.datasources.DonkiDataSource
import com.feature.donki.repositories.DonkiRepository
import com.feature.donki.service.DonkiService
import com.feature.donki.service.getApi
import com.feature.donki.viewmodels.CoronalMassEjectionViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val donkiServiceModule = module {
    factory { getApi(DonkiService::class.java) }
    factory { DonkiDataSource(get()) }
    factory { DonkiRepository(get()) }
    viewModel {
        CoronalMassEjectionViewModel(
            get(),
            Dispatchers.IO
        )
    }
}