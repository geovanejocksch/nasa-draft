package com.feature.donki.models

data class CoronalMassEjection(
    val activeRegionNum: Int,
    val activityID: String,
    val catalog: String,
    val cmeAnalyses: List<CmeAnalyse>,
    val instruments: List<Instrument>,
    val linkedEvents: List<LinkedEvent>,
    val note: String,
    val sourceLocation: String,
    val startTime: String
)

data class Instrument(
    val displayName: String,
    val id: Int
)

data class LinkedEvent(
    val activityID: String
)

data class CmeAnalyse(
    val enlilList: Any,
    val halfAngle: Double,
    val isMostAccurate: Boolean,
    val latitude: Double,
    val levelOfData: Int,
    val longitude: Double,
    val note: String,
    val speed: Double,
    val time21_5: String,
    val type: String
)