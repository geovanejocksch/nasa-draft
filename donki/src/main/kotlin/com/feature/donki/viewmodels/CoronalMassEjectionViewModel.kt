package com.feature.donki.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.feature.donki.models.CoronalMassEjection
import com.feature.donki.repositories.DonkiRepository
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class CoronalMassEjectionViewModel(
    private val repository: DonkiRepository,
    private val coroutineContext: CoroutineContext
) : ViewModel() {

    private val _coronalMassEjectionList = MutableLiveData<List<CoronalMassEjection>>()
    val coronalMassEjectionList: LiveData<List<CoronalMassEjection>>
        get() = _coronalMassEjectionList

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    fun getCoronalMassEjectionViewModel(startDate: String, endDate: String) {
        CoroutineScope(coroutineContext).launch {
            try {
                val response = repository.getCoronalMassEjectionList(startDate, endDate)
                _coronalMassEjectionList.postValue(response)
            } catch (e: Exception) {
                _error.postValue(e.message)
            }
        }
    }

}